package com.pureplanet.tests;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.appium.support.AppiumDriverFactory;
import com.appium.support.DataProviderUtils;
import com.appium.support.PureEnergeyPageFactory;

/**
 * Hello world!
 *
 */

public class App {

   @Parameters({"deviceConfig"})
   @Test
	public static void logintoTheApp(String deviceInfo)
			throws MalformedURLException, InterruptedException {
	
		AppiumDriver<MobileElement> driver = null;
		driver = AppiumDriverFactory.get(deviceInfo);

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		PureEnergeyPageFactory ele=new PureEnergeyPageFactory(driver);
		
		//Meter Reading Day Reading, Night Reading & Gas Reading
		String[] meterReadings={"1230","350","1200"};
				
		//Calling the submitMeterReadings method From PureEnergeyPageFactory class
		ele.submitMeterReadings("189825",meterReadings);
		
		//ele.login("test@purepla.net");
					
	    //ele.clickOnGetQuote();
		
		driver.quit();

	} 
}
