package com.appium.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class PropertiesReader {

	private Properties properties = null;
	private static PropertiesReader propReader = null;

	private static Logger log = Logger.getLogger(PropertiesReader.class);

	/**
	 * Default Constructor to load Config.properties
	 */
	private PropertiesReader() {
		properties = loadProperties();

	}

	/**
	 * Parameterized constructor to load given property file
	 * 
	 * @param propertyFile
	 *            - Property file name without extension
	 */
	private PropertiesReader(String propertyFile) {
		properties = loadProperties(propertyFile);

	}

	/**
	 * To default property file config.properties
	 * 
	 * @return Properties -
	 */
	private Properties loadProperties() {
		
		
		File propFile = new File(".\\src\\main\\resources\\config.properties");
		System.out.println(propFile.exists());
		FileInputStream fis = null;
		Properties props = new Properties();
		try {
			fis = new FileInputStream(propFile);
			props.load(fis);
			fis.close();

		} catch (FileNotFoundException e) {
			log.error("config.properties file is missing at specified location or corrupted: "
					+ e.getMessage());

		} catch (IOException e) {
			log.error("config.properties file cann't read due to : "
					+ e.getMessage());
		}

		return props;

	}

	/**
	 * To load properties from given property file name without extension
	 * 
	 * @param propertyFile
	 *            - Property File Name without Extension
	 * @return Properties - All properties available in property file
	 */
	private Properties loadProperties(String propertyFile) {

		File propFile = new File(".\\src\\main\\resources\\" + propertyFile+".properties");
		FileInputStream fis = null;
		Properties props = new Properties();
		try {
			fis = new FileInputStream(propFile);
			props.load(fis);
			fis.close();

		} catch (FileNotFoundException e) {
			log.error(propertyFile
					+ " file is missing at specified location or corrupted: "
					+ e.getMessage());

		} catch (IOException e) {
			log.error(propertyFile + " file cann't read due to : "
					+ e.getMessage());
		}

		return props;
	}

	/**
	 * To get given property file instance of EnvirionmentPropertyReader
	 * 
	 * @param propertyFile
	 *            - Property File name without
	 * @return EnvironmentPropertiesReader - Property file instance
	 */
	public static PropertiesReader getInstance() {
		if (propReader == null) {
			propReader = new PropertiesReader();
		}
		return propReader;
	}
	
	/**
	 * To get given property file instance of EnvirionmentPropertyReader
	 * 
	 * @param propertyFile
	 *            - Property File name without
	 * @return EnvironmentPropertiesReader - Property file instance
	 */
	public static PropertiesReader getInstance(String propertfile) {
		PropertiesReader propReader=null;
		if (propReader == null) {
			propReader = new PropertiesReader(propertfile);
		}
		return propReader;
	}

	/**
	 * To get given property file instance of EnvirionmentPropertyReader
	 * @param propertyFile - Property File name without
	 * @return EnvironmentPropertiesReader - Property file instance
	 */

	public String getProperty(String key) {

		return properties.getProperty(key);
	}

	/**
	 * To set property with given key and value instance
	 * 
	 * @param key
	 *            - Key String
	 * @param value
	 *            - Value String
	 */
	public void setProperty(String key, String value) {
		properties.setProperty(key, value);
	}
	
	public int getIntProperty(String key)
	{
		return Integer.parseInt(properties.getProperty(key));
	}

	/**
	 * To verify given key is available in properties
	 * 
	 * @param key
	 *            - Key String
	 * @return boolean - true/false
	 */
	public boolean hasProperty(String key) {
		
		//StringUtils.isBlank(properties.getProperty(key));
		
		if (properties.getProperty(key).isEmpty()
				|| properties.getProperty(key).equals(null)
				|| properties.getProperty(key).equals(""))
			return false;
		else
			return true;
	}


}
