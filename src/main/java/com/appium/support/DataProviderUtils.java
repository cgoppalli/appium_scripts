package com.appium.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;


public class DataProviderUtils {
	
	private static PropertiesReader configProperty=PropertiesReader.getInstance();
	
	
	@DataProvider(parallel=true)
	public static Iterator<Object[]> parallelTestDataProvider(ITestContext context)
	{
		List<Object[]> dataTobeReturned=new ArrayList<Object[]>();
		
		List<String> deviceName=null;
		Iterator<String> deviceNameIt=null;
		
		List<String> udid=null;
		Iterator<String> udidIt=null;
		
		String driverInitializationInfo=null;
		
		boolean runEmulator=false;

		
		if(configProperty.getProperty("runEmulator").equals("true"))  runEmulator=true;
		
	
		if(runEmulator)
		{
			deviceName=Arrays.asList(configProperty.getProperty("mobileDeviceName").split("\\|"));
			udid=Arrays.asList(configProperty.getProperty("udid").split("\\|"));
			
			deviceNameIt=deviceName.iterator();
			udidIt=udid.iterator();
			
			while(deviceNameIt.hasNext())
			{
				driverInitializationInfo=deviceNameIt.next()+"&"+udidIt.next();
				
				dataTobeReturned.add(new Object[]{driverInitializationInfo});
			}
						
		}	
		return dataTobeReturned.iterator();	
		
	}

}
