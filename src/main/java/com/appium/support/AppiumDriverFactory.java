package com.appium.support;

import java.io.File;
import java.net.URL;
import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.functions.ExpectedCondition;
import io.appium.java_client.remote.MobileCapabilityType;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;



public class AppiumDriverFactory {
		
	private static Logger log=Logger.getLogger(AppiumDriverFactory.class);
	static PropertiesReader configProperty=PropertiesReader.getInstance();

	/**
	 * To get a new instance of app driver using a particular deviceConfig
	 * 
	 * @param udid -
	 * @return driver -
	 */
	public static AppiumDriver<MobileElement> get(String deviceConfig) {
		AppiumDriver<MobileElement> driver = null;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		try {
			
			String appiumURL=configProperty.getProperty("nodeURL");
			capabilities = getDesiredCapabilities(deviceConfig);
			
			driver = new AndroidDriver<MobileElement>(new URL(appiumURL), capabilities);

		} catch (Exception e) {
			log.error("Could not create a driver session. Trying again...");
		} 
		return driver;
	}
	
	
	/**
	 * To set desired capabilities using configured parameters
	 * 
	 * @param udid
	 *            - udid to get a particular device/ blank or null to get any
	 *            available device
	 * @return capabilities
	 */
	private static DesiredCapabilities getDesiredCapabilities(String deviceInfo) {
		String[] deviceInitializationInfo=deviceInfo.split("\\|");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		File appLink=new File(System.getProperty("user.dir")+File.separator+configProperty.getProperty("App"));

		capabilities.setCapability("automationName", "uiautomator2");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,deviceInitializationInfo[0]);
		capabilities.setCapability("udid", "emulator-"+deviceInitializationInfo[1]);
		capabilities.setCapability(MobileCapabilityType.APP,appLink);
		capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "60");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("appPackage","com.pureplanet.pureplanettest");
		capabilities.setCapability("appActivity", "com.pureplanet.pureplanettest.MainActivity");
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("appWaitDuration", 60000);
		
		return capabilities;
	}
	
	
	
	
}
