package com.appium.support;


import java.util.List;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class PureEnergeyPageFactory extends LoadableComponent<PureEnergeyPageFactory>{
	
	private AppiumDriver<MobileElement> driver=null;
	private static PropertiesReader configProperty=PropertiesReader.getInstance();
	
	@AndroidFindBy(accessibility="")
	MobileElement hambugerMenu;
	
	@AndroidFindBy(accessibility="Get a quote")
	MobileElement getQuoteLink;
	
	@AndroidFindBy(accessibility="Start >")
	MobileElement startLink;
	
	@AndroidFindBy(xpath="//android.view.View[@content-desc = 'Log in']")
	MobileElement loginLink;
	
	@AndroidFindBy(xpath="//android.view.View[@content-desc='Login']")
	MobileElement loginHeading; 
	
	@AndroidFindBy(className="android.widget.EditText")
	MobileElement loginEmail;
	
	@AndroidFindBy(xpath="//android.widget.Button[@content-desc='Login']")
	MobileElement loginBtn;
	
	@AndroidFindBy(accessibility="Email address not found.")
	MobileElement invalidEmail;
	
	@AndroidFindBy(accessibility="Backdoor Login")
	MobileElement backdoorLink;
	
	@AndroidFindBy(accessibility="METER READINGS")
	MobileElement meterReadingHeading;
	
	@AndroidFindBy(className="android.widget.EditText")
	MobileElement memberID;
	
	@AndroidFindBy(id="android:id/button1")
	MobileElement okBtn;
	
	@AndroidFindBy(className="android.widget.EditText")
	List<MobileElement> meterReadingFields;
	
	@AndroidFindBy(accessibility="Next")
	MobileElement nextBtn;
	
	@AndroidFindBy(accessibility="Tap below to enter your new readings.")
	MobileElement torchBtn;
	
	@AndroidFindBy(className="android.widget.EditText")
	MobileElement gasReading;
	
	@AndroidFindBy(accessibility="Submit")
	MobileElement submitBtn;
	
	@AndroidFindBy(accessibility="Close")
	MobileElement closeBtn;
	
	@AndroidFindBy(accessibility="We've already received your meter reading today - so you're all up to date.")
	MobileElement alreadyReceived;
	
	@AndroidFindBy(id="scroller-body")
	MobileElement scrollerPopup;
	
	WebDriverWait wait;
	TouchAction act;

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
		
	public PureEnergeyPageFactory(AppiumDriver<MobileElement> driver)
	{
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(this.driver), this);
		
		wait=new WebDriverWait(this.driver,60);
		act=new TouchAction(this.driver);
	}
	
	public void clickUsingCordinates(int xAxis, int yAxis)
	{
		act.tap(xAxis, yAxis).perform();
	}
	
	public void clickOnHamburgerMenu() throws InterruptedException
	{
		wait.until(ExpectedConditions.visibilityOf(hambugerMenu));
		System.out.println("Home page is loaded");
		hambugerMenu.click();
		System.out.println("Clicked on Hamburger Menu");
		Thread.sleep(2000);	
	}
	
	

	public void submitMeterReadings(String loginID,String meterReadings[]) throws InterruptedException
	{			
		
		//Clicking on Debug Link
		clickOnHamburgerMenu();
		clickUsingCordinates(configProperty.getIntProperty("X-Coord"), configProperty.getIntProperty("Debug"));
        System.out.println("Clicked on Debug Link using its coorinates");
        
        //Clicking on Backdoor Link        
		wait.until(ExpectedConditions.visibilityOf(backdoorLink));
        backdoorLink.click();
        System.out.println("Clicked on Backdoor link");
        
       
        //Providing MemberID
		wait.until(ExpectedConditions.visibilityOf(memberID));
		memberID.setValue(loginID);
        System.out.println("Provided valid MemberID");
		okBtn.click();
        System.out.println("Clicked on OK Btn");
        
        //Waiting for Home Page
        clickOnHamburgerMenu();
        
        //Clicking on SendAMeterReading Link
		clickUsingCordinates(configProperty.getIntProperty("X-Coord"), configProperty.getIntProperty("SendAMeterReading"));
		System.out.println("Clicked on Send A Meter Reading Link");
		//Waiting for Meter Reading Page
		wait.until(ExpectedConditions.visibilityOf(meterReadingHeading));
		
		//Providing Day & Night Reading
		int i=0;
		for(MobileElement e:meterReadingFields)
		{
			e.sendKeys(meterReadings[i]); i++;
		}
        System.out.println("Entered Meter Reading values successfully");
        
        //Scrolling until Next Btn is visible
        act.tap(167, 1485).perform();
        Thread.sleep(2000);           
        act.press(1221, 1905).moveTo(1246, -1905).release().perform();
        System.out.println("Scrolling until Next button is visible");
        Thread.sleep(3000);
        
        //Clicking On Next Button 
        nextBtn.click();
        System.out.println("Clicked on Next Btn");
        
        //Providing Gas Reading value
        Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOf(gasReading));
        gasReading.sendKeys(meterReadings[i]);
        System.out.println("Provided Gas Reading Values..");
        act.tap(167, 1485).perform();
        Thread.sleep(1000);
        nextBtn.click();
        System.out.println("Submitted the Gas Reading values..");

        
        Thread.sleep(3000);
        //Scrolling until Submit Btn is visible
        act.press(910, 1881).moveTo(910, -1881).release().perform();
        Thread.sleep(2000);
        act.tap(910, 1881).perform();
        System.out.println("Scrolling until Submit button is visible");
        wait.until(ExpectedConditions.visibilityOf(submitBtn));
        
        //Verifying submitting values
    	i=0;
		for(MobileElement e:meterReadingFields)
		{
			Assert.assertEquals(e.getText(), meterReadings[i]); 
			System.out.println("Meter Readings As Expected::::"+e.getText()+" "+ meterReadings[i]); i++;
		}
		
      //Verifying Meter Reading review
        submitBtn.click();
       System.out.println("Submitting meter reading values..");
        
       Thread.sleep(8000);

        clickUsingCordinates(346,1791);
        
        Thread.sleep(4000);
		
        //Waiting for Home Page
	   clickOnHamburgerMenu();
        //Clicking on SendAMeterReading Link
       clickUsingCordinates(configProperty.getIntProperty("X-Coord"), configProperty.getIntProperty("SendAMeterReading"));
        wait.until(ExpectedConditions.visibilityOf(alreadyReceived));
        System.out.println("Meter values are received Successfully");
            
	    //Waiting for Home Page
	    clickOnHamburgerMenu();
	    //Clicking on Log out Link
		clickUsingCordinates(configProperty.getIntProperty("X-Coord"), configProperty.getIntProperty("Logout"));
        wait.until(ExpectedConditions.visibilityOf(loginLink));
		System.out.println("Logged out from the application successfully..");
	}
	
	public void clickOnGetQuote() throws InterruptedException
	{
		wait.until(ExpectedConditions.visibilityOf(getQuoteLink));
		
		getQuoteLink.click();
		System.out.println("Clicked on Get Quote Link");

		startLink.click();
		System.out.println("Clicked on Start > Link");

		hambugerMenu.click();
	}
	
	public void login(String email) throws InterruptedException
	{
		wait.until(ExpectedConditions.visibilityOf(loginLink));
		
		loginLink.click();
		System.out.println("Login Popup is loaded successfully");
		
		wait.until(ExpectedConditions.visibilityOf(loginEmail));
		loginEmail.sendKeys(email);
		System.out.println("Provided login email");
	
		loginHeading.click();
		
	    Thread.sleep(2000);
		loginBtn.click();
		System.out.println("Clicked on login Btn");

			try{
				if(invalidEmail.isDisplayed())
					System.out.println("Email isn't valid");
			}catch(NoSuchElementException e)
			{
				System.out.println("Email is valid & logged in successfully");
			}
	}

}
